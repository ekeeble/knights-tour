/*************************************************************************//**
 * @file
 * @brief Header file containing the includes for this program, and the menu
 * choices.
 *****************************************************************************/
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

#ifndef _MENU_H_
#define _MENU_H_
/*!
*@brief Displays the menu and allows easy manipulation of it
*/
class menu
{
    public:
        menu( );
        menu ( menu &aMenu );
        menu( vector<string> &menuList );

        /*void subMenu( vector<string> &v, int argc, char *argv[] );*/
        void setUpMainMenu ( menu &aMenu );

        bool addMenuItem ( string item, int pos );
        bool removeMenuItem ( int pos );
        bool updateMenuItem ( string item, int pos );

        void printMenu ( );
        int getMenuSelection ( bool withMenu = true );
        int size( ); 
        void clear( );
        
    private:
        vector<string> theMenu;

};

#endif
