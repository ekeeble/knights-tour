/**************************************************************************//**
* @file
* @brief Solves the knights tour problem given board size and starting point
*
* @mainpage Program 3 - The Knight's Tour
*
* @section course_section Course Information
*
* @author Erica Keeble
*
* @date 4/16/18
*
* @par Instructor:
*       Professor Roger Schrader
*
* @par Course:
*       CSC 215 - Section 1 - 11:00am
*
* @par Location
*       Classroom Building - Room 205E
*
* @section program_section Program Information
*
* @details Reads in from the command line either a tour file, -fancy,
* both of those, or neither. If a tour file was given, it will open the file
* and solve each of the tours given in that file, outputting them to another 
* file. If a tour was not given, a menu will display and give the user the 
* options to enter a new starting point, new board size, and to solve the tour.
* The solved tours will be outputted to the screen. If -fancy is included, then
* the tours will not be outputted in a fancy formatting, just regular.
*
* @section compile_section Compiling and Usage
*
* @par Compiling Instructions:
*               Command line arguments need to include either a tour file, 
*               -fancy, both of those (with tour file first), or neither.
*
* @par Usage
* @verbatim
* c:\> prog3.exe 
* c:\> prog3.exe tourfile
* c:\> prog3.exe -fancy
* c:\> prog3.exe -fancy tourfile
* d:\> c:\bin\prog3.exe
* @endverbatim
*
* @section todos_bugs_modification_section ToDo, Bugs, and Modifications
*
* @bug Does not update the starting point or board size in the menu. It does 
*      update them for calculating the tour, though.
*
* @par Modification and Development Timeline:
*   <a href="https://gitlab.mcs.sdsmt.edu/7431673/csc215s18programs
/commits/master">Click here for commit log.</a>
*
********************************************************************************/

/*HELP: Doxygen isnt updating my changes when I generate documentation
*/
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include "menu.h"

using namespace std;

void createMenu(menu &theMenu);
void menuChoice1(int &boardSize, int startPt[], menu &theMenu);
void menuChoice2(int &boardSize, int startPt[], menu &theMenu);
void outputTour(int **solvedTour, int boardSize, ostream &out, bool solved,
    int startPt[], int numToursOut);
bool solveTour(int boardSize, int startPt[], int **&solvedTour);
bool openFiles(string fileName, ifstream &tourFile, ofstream &solvedTourFile);
bool dynamTourArr(int **&solvedTour, int &boardSize);
bool dynamUsedArr(int **&used, int &boardSize, int startPt[]);
void recursionYay(bool &solved, int cRow, int cCol, int **used, 
    int **solvedTour, int index, int n);
void insides(int cRow, int cCol, int **used, int **solvedTour, int index,
    int n, bool &solved);


/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Main
*
* @param[in]       argc - number of arguments in the command line
* @param[in]       argv[] - array containing command line arguments
*
* @returns 0 if successful 
* @returns -1 if incorrect command line arguments
******************************************************************************/
int main(int argc, char* argv[])
{
    int boardSize = 0;
    int startPt[2] = { 7,7 };
    int **solvedTour = nullptr;

    //separate main into "file was given", or "file was not given"
    //file was not given
    if (argc == 1 || (argc == 2 && strcmp(argv[1], "-fancy") == 0))
    {
        //set up menu
        vector<string> temp;
        menu theMenu(temp);
        createMenu(theMenu);

        int menuChoice;
        bool solved;
        boardSize = 8;

        //display menu with default board size of 8x8, starting point (7,7)
        //and prompt for user to change values
        do
        {
            theMenu.printMenu();
            
            cout << "Enter choice: ";
            cin >> menuChoice;

            switch (menuChoice)
            {
            case 1: //change board size
                menuChoice1(boardSize, startPt, theMenu);
                break;
            case 2: //change starting point
                menuChoice2(boardSize, startPt, theMenu);
                break;
            case 3: //solve tour
                solved = solveTour(boardSize, startPt, solvedTour);
                break;
            default:
                cout << "Invalid menu option." << endl << endl;
                break;
            }

        } while (menuChoice != 3);  

        //once tour is solved, output it to screen
        if (argc == 2)
        {
            //output fancy
            cout << "No fancy :(" << endl;
            outputTour(solvedTour, boardSize, cout, solved, startPt, 1);
        }
        else
            outputTour(solvedTour, boardSize, cout, solved, startPt, 1);

        //delete array
        for (int i = 0; i < boardSize; ++i)
            delete[] solvedTour[i];
        delete[] solvedTour;

        return 0;
    }
    //file was given
    else if((argc == 3 && strcmp(argv[1], "-fancy") == 0) || argc == 2)
    {
        ifstream tourFile;
        ofstream solvedTourFile;
        int numToursOut = 1;

        //open given input file, and output file
        //if fancy, open argv[2]. If not fancy, open argv[1]
        if (argc == 3)
        {
            if (!openFiles(argv[2], tourFile, solvedTourFile))
                return -1;
        }
        else
        {
            if (!openFiles(argv[1], tourFile, solvedTourFile))
                return -1;
        }

        //loop, while we can read in size and starting point
        while (tourFile >> boardSize >> startPt[0] >> startPt[1])
        {            
            //solve the tour
            int solved;
            solved = solveTour(boardSize, startPt, solvedTour);
            
            //output the result, depending on if fancy was included or not
            if (argc == 2)
            {
                outputTour(solvedTour, boardSize, solvedTourFile, solved, startPt,
                    numToursOut);
            }
            else
            {
                solvedTourFile << "No fancy :(" << endl;
                outputTour(solvedTour, boardSize, solvedTourFile, solved, startPt,
                    numToursOut);
            }
            numToursOut++;
        }

        //close files
        tourFile.close();
        solvedTourFile.close();

        //delete array
        for (int i = 0; i < boardSize; ++i)
            delete[] solvedTour[i];
        delete[] solvedTour;
    }
    else
    {
        cout << "Incorrect Usage. Possible Usages:" << endl
            << "    prog3.exe tourfile" << endl
            << "    prog3.exe -fancy tourfile" << endl
            << "    prog3.exe -fancy" << endl
            << "    prog3.exe " << endl;

        return -1;
    }
    return 0;
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Creates the menu
*
* @param[out]       theMenu - the main menu that is displayed to the user
*
******************************************************************************/
void createMenu(menu &theMenu)
{
    //create 2D vector with all menu options
    vector<string> temp;

    temp.push_back("    Change board size from 8x8");
    temp.push_back("    Change starting location from (7,7)");
    temp.push_back("    Exit and Solve tour");

    //create new menu with it
    menu aMenu(temp);
    theMenu = aMenu;
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* This function will update the board size, and update the menu with the new
* board size.
*
* @param[out]       boardSize - size of the board we will be solving the tour of
* @param[out]       startPt - starting point on the board, so where we will start
*                             solving the tour
* @param[out]       theMenu - the main menu that is displayed to the user
*
******************************************************************************/
void menuChoice1(int &boardSize, int startPt[], menu &theMenu)
{
    //prompt for new board size
    cout << "New board size (form: x)   ";
    cin >> boardSize;

    //make sure to error check
    while (boardSize < (startPt[0] + 1) && (boardSize < startPt[1] + 1))
    {
        cout << "Invalid board size. Try again: ";
        cin >> boardSize;
    }   
    cout << endl;

    //update menu
    string updateMenu = "    Change board size from ";
    updateMenu.push_back('9');
    updateMenu.push_back('x');
    updateMenu.push_back('9');
    
    theMenu.updateMenuItem(updateMenu, 0);
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* This function will update the start point, and update the menu with the new
* start point.
*
* @param[out]       startPt - starting point on the board, so where we will start 
*                             solving the tour
* @param[out]       boardSize - size of the board we will be solving the tour of
* @param[out]       theMenu - the main menu that is displayed to the user
*
******************************************************************************/
void menuChoice2(int &boardSize, int startPt[], menu &theMenu)
{
    //prompt for new startpt
    cout << "New starting point." << endl << "Row:  ";
    cin >> startPt[0];
    cout << "Column:    ";
    cin >> startPt[1];

    //make sure to error check
    while (boardSize < (startPt[0] + 1) || (boardSize < startPt[1] + 1))
    {
        cout << "Invalid starting point. Try again." << endl;
        cout << "Row: ";
        cin >> startPt[0];
        cout << "Col: ";
        cin >> startPt[1];
    }
    cout << endl;

    //update menu
    string menuUpdate = "    Change starting location from (";
    menuUpdate.push_back((char)startPt[0]);
    menuUpdate.push_back(',');
    menuUpdate.push_back((char)startPt[1]);
    menuUpdate.push_back(')');
    
    theMenu.updateMenuItem(menuUpdate, 1);
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Outputs the solved tour. 
*
* @param[in]      out - form of output. Like cout or fout.
* @param[in]      solvedTour - a 2d array that holds the first solution to the 
*                               tour with our parameters
* @param[in]      boardSize - size of the board we will be solving the tour of
* @param[in]      solved - boolean that is true if there was a solution found
*                                to the tour, false otherwise
* @param[in]      startPt - starting point on the board, so where we will start
*                                solving the tour
* @param[in]      numToursOut - the number of tours that have been outputted to
*                                the file so far
*
******************************************************************************/
void outputTour(int **solvedTour, int boardSize, ostream &out, bool solved,
    int startPt[], int numToursOut)
{
    //tour header
    out << "Tour : # " << numToursOut << endl << setw(10)
        << boardSize << " X " << boardSize << " starting ("
        << startPt[0] << ", " << startPt[1] << ")" << endl;

    //output board if it was solved
    if (solved == true)
    {
        for (int i = 0; i < boardSize; i++)
        {
            out << endl << setw(10) << " ";
            for (int j = 0; j < boardSize; j++)
                out << setw(3) << solvedTour[i][j];
        }
        out << endl << endl << endl;
    }
    else
    {
        out << endl << setw(37) << "No solution for this case." 
            << endl << endl;
    }
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Solves the tour with the given parameters
*
* @param[in]       startPt - starting point on the board, so where we will start
*                             solving the tour
* @param[in]       boardSize - size of the board we will be solving the tour of
* @param[out]      solvedTour - a 2d array that holds the first solution to the
*                               tour with our parameters
*
* @returns true if successful
* @returns false otherwise
*
******************************************************************************/
bool solveTour(int boardSize, int startPt[], int **&solvedTour)
{
    //dynamically allcate solvedTour array with the given size
    if (!dynamTourArr(solvedTour, boardSize))
        return false;

    //dynamically allcate "used" array with the given size
    int **used;
    if (!dynamUsedArr(used, boardSize, startPt))
        return false;

    int cRow = startPt[0] + 2;
    int cCol = startPt[1] + 2;
    int n = boardSize;
    bool solved = false;
    int index = 1;
    solvedTour[startPt[0]][startPt[1]] = 1;
    used[cRow][cCol] = 1;  
    
    recursionYay(solved, cRow, cCol, used, solvedTour, index + 1, n);

    if (solved == true)
        return true;
    else
        return false;
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Opens the given input file, and an output file we call "Solutions.tours"
*
* @param[in]      fileName - a string that holds the name of the file given in
*                            the command line argument
* @param[out]     tourFile - input file object
* @param[out]     solvedTourFile - output file object
*
* @returns true if files opened successfully
* @returns false otherwise
*
******************************************************************************/
bool openFiles(string fileName, ifstream &tourFile, ofstream &solvedTourFile)
{
    tourFile.open(fileName);
    if (!tourFile.is_open())
    {
        cout << "Input file did not open." << endl;
        return false;
    }
    
    solvedTourFile.open("Solutions.tours", ofstream::app);
    if (!solvedTourFile.is_open())
    {
        cout << "Output file did not open." << endl;
        return false;
    }

    return true;
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Dynamically allocates the array solvedTour and fills with 0's
*
* @param[in]      boardSize - size of the board we will be solving the tour of
* @param[in]      solvedTour - a 2d array that holds the first solution to the
*                               tour with our parameters
*
* @returns true if successfully allocated array
* @returns false otherwise
*
******************************************************************************/
bool dynamTourArr(int **&solvedTour, int &boardSize)
{
    //dynamically allocate array
    solvedTour = new (nothrow) int *[boardSize];
    if (solvedTour == nullptr)
    {
        cout << "Not enough memory." << endl;
        return false;
    }
    //fill solvedTour with 0's
    for (int i = 0; i < boardSize; i++)
    {
        solvedTour[i] = new (nothrow) int[boardSize];
        if (solvedTour == nullptr)
        {
            cout << "Not enough memory." << endl;
            return false;
        }
        for (int j = 0; j < boardSize; j++)
            solvedTour[i][j] = 0;
    }
    return true;
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Dynamically allocates the used array, and fills with appropriate 0's and -1's
*
* @param[in]      boardSize - size of the board we will be solving the tour of
* @param[in]      used - array that keeps track of which spots have been used
* @param[in]      startPt - starting point on the board, so where we will start
*                             solving the tour
*
* @returns true if successfully allocates array
* @returns false otherwise
*
******************************************************************************/
bool dynamUsedArr(int **&used, int &boardSize, int startPt[])
{
    //dynamically allocate array
    used = new (nothrow) int *[boardSize + 4];
    if (used == nullptr)
    {
        cout << "Not enough memory." << endl;
        return false;
    }

    for (int i = 0; i < boardSize + 4; i++)
    {
        used[i] = new (nothrow) int[boardSize + 4];
        if (used == nullptr)
        {
            cout << "Not enough memory." << endl;
            return false;
        }

        //fill whole things with -1's
        for (int j = 0; j < boardSize + 4; j++)
            used[i][j] = -1;
    }

    //then fill correct spots with 0's
    for (int i = 2; i < boardSize + 2; i++)
    {
        for (int j = 2; j < boardSize + 2; j++)
            used[i][j] = 0;
    }

    return true;
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Recursive call to solve the tour with the given parameters
*
* @param[in]       solved - boolean, tells if we have solved the tour yet
* @param[in]       cCol - current column
* @param[in]       cRow - current row
* @param[in]       used - array that keeps track of which spots have been used
* @param[out]      solvedTour - a 2d array that holds the first solution to the
*                               tour with our parameters
* @param[in]       index - keeps track of how many layers deep we are in recursion
* @param[in]       n - board size
*
******************************************************************************/
void recursionYay(bool &solved, int cRow, int cCol, int **used,
    int **solvedTour, int index, int n)
{
    //base case, for if we solve the tour
    if (index == n * n + 1)
    {
        solved = true;
        return;
    }

    //if solved is true, return 
    if (solved == true)
        return;    

    //can knight move to position a?
    if (used[cRow - 2][cCol + 1] == 0 && solved == false)
        insides(cRow - 2, cCol + 1, used, solvedTour, index, n, solved);
    //pos b?
    if (used[cRow - 1][cCol + 2] == 0 && solved == false)
        insides(cRow - 1, cCol + 2, used, solvedTour, index, n, solved);
    //pos c?
    if (used[cRow + 1][cCol + 2] == 0 && solved == false)
        insides(cRow + 1, cCol + 2, used, solvedTour, index, n, solved);
    //pos d?
    if (used[cRow + 2][cCol + 1] == 0 && solved == false)
        insides(cRow + 2, cCol + 1, used, solvedTour, index, n, solved);
    //pos e?
    if (used[cRow + 2][cCol - 1] == 0 && solved == false)
        insides(cRow + 2, cCol - 1, used, solvedTour, index, n, solved);
    //pos f?
    if (used[cRow + 1][cCol - 2] == 0 && solved == false)
        insides(cRow + 1, cCol - 2, used, solvedTour, index, n, solved);
    //pos g?
    if (used[cRow - 1][cCol - 2] == 0 && solved == false)
        insides(cRow - 1, cCol - 2, used, solvedTour, index, n, solved);
    //pos h?
    if (used[cRow - 2][cCol - 1] == 0 && solved == false)
        insides(cRow - 2, cCol - 1, used, solvedTour, index, n, solved);

    //if it tries all possible combos from starting point, return false
    if (index == 2 && solved != true)
    {
        solved = false;
        return;
    }
    
    //if it tries through all of the positions, return
    return;
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Called inside recursive call. Updates used and solvedTour, and calls the next
* recursion. Then when it comes back from that recursion, it un-updates used.
*
* @param[in]       solved - boolean, tells if we have solved the tour yet
* @param[in]       cCol - current column
* @param[in]       cRow - current row
* @param[in]       used - array that keeps track of which spots have been used
* @param[out]      solvedTour - a 2d array that holds the first solution to the
*                               tour with our parameters
* @param[in]       index - keeps track of how many layers deep we are in recursion
* @param[in]       n - board size
*
******************************************************************************/
void insides(int cRow, int cCol, int **used, int **solvedTour, int index, 
    int n, bool &solved)
{
    //mark used[][] to 1
    used[cRow][cCol] = 1;

    //mark solvedTour to whatever index
    solvedTour[cRow-2][cCol-2] = index;

    /*recursive call, pass in cRow and cCol with +- whatever to indicate their
      new positions */
    recursionYay(solved, cRow, cCol, used, solvedTour, index + 1, n);

    //mark used to 0 and move on
    used[cRow][cCol] = 0;
}
