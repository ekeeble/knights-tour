/*************************************************************************//**
 * @file
 * @brief cpp file containing the code for functions in the menu class
 *
 *****************************************************************************/
#include "menu.h"

using namespace std;

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* The constructor creates a blank menu.
*
******************************************************************************/
menu::menu( )
{
    theMenu[0] = '\0';
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Sets the menu equal to a predefined menu class to be copied. 
******************************************************************************/
menu::menu ( menu &aMenu )
{
    aMenu = theMenu;
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Sets the menu equal to a predefined vector containing the menu entries 
*
******************************************************************************/
menu::menu( vector<string> &menuList )
{
    theMenu = menuList;
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* Sets up the main menu on the screen
*
* @param[in,out]      theMenu - vector of strings to hold the menu entries. 
******************************************************************************/
void menu::setUpMainMenu(menu &theMenu)
{
    int i;
    vector<string> v = { 
    "   1) Change board size from 8x8", 
    "   2) Change starting location from (7,7)", 
    "   3) Exit and Solve tour"
    };

    for (i = 0; i<v.size(); i++)
    {
        theMenu.addMenuItem(v.at(i), theMenu.size() + 1);
    }
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* This function will place a menu entry at the given location if the
* position (starts at 1) provided is valid. If the entry is stored, true
* will be returned. Otherwise false will be returned.
*
* @param[in]          item - the menu item that is being added
* @param[in]          pos - position where the menu item will be added
*
* @returns true if the entry is stored
* @returns false otherwise
*****************************************************************************/
bool menu::addMenuItem ( string item, int pos )
{
    //if position is somewhere in the middle or at the beginning of theMenu
    if ( pos <= theMenu.size() )
    {
        //insert item into the menu
        theMenu.insert ( theMenu.begin() + pos, item );
        
        return true;
    }
    
    //if the menu is empty, add item as the first element
    else if ( theMenu.size() == 0 )
    {
        theMenu.push_back ( item );
    }
    
    else
    {
        cout << "Invalid position" << endl;
    }
    
    return false;
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* This function will remove the menu item at the given position (starts at 1).
* If the item is removed, true will be returned. If it fails,
* false will be returned.
*
* @param[in]          pos - position in the menu where we will remove item
*
* @returns true if the item was removed
* @returns false otherwise
******************************************************************************/
bool menu::removeMenuItem ( int pos )
{
    //if the position is within the number of items in the menu
    if ( pos <= theMenu.size() )
    {
        theMenu.erase ( theMenu.begin() + pos );
        return true;
    }
    
    else
    {
        return false;
    }
}


/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* will update a menu location given by position (starts at 1) with
* the new entry phrase. If successfully updated, true will be returned.
* Otherwise false is returned.
*
* @param[in]          pos - determines where the menu item will be added
* @param[out]         item - the string containing the new menu item
*
* @returns true if successfully updated
* @returns false otherwise
******************************************************************************/
bool menu::updateMenuItem ( string item, int pos )
{
    if ( pos >= 1 && pos <= theMenu.size() )
    {
        theMenu.at ( pos ) = item;
        return true;
    }
    
    else
    {
        return false;
    }
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description: prints the options stored in the menu class
*
******************************************************************************/
void menu::printMenu()
{
    //go through all values and enumerate them
    //can handle stupidly large sizes of lists
    for (long long unsigned int i = 0; i < size(); i++)
    {
        cout << i + 1 << ") " << theMenu.at(i) << endl;
    }
}


/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* If a value of true is passed in to the function for withMenu, the menu
* will be displayed before prompting for any value from the user. If
* the value is not an entry for the menu, an error message will be displayed,
* and the function will continue prompting for an integer that represents
* a valid menu choice. Remember, counting starts at 1. Upon successful
* data entry, return this value.
*
* @param[in,out]      withMenu - a bool variable. If true, display the menu
*                                before prompting for an input selection.
*
* @returns the user's menu selection
*****************************************************************************/
int menu::getMenuSelection ( bool withMenu )
{

    int selection = 0;      //user's menu selection
    bool valid = false;
    
    if ( withMenu == true )
    {
        printMenu();
    }
    
    //repeat until input is valid
    while ( valid == false )
    {
        //ask user for their menu selection
        cout << "Enter choice: ";
        cin >> selection;
        
        if ( selection <= theMenu.size() )
        {
            valid = true;
        }
        
        else
        {
            cout << "Invalid entry. Try again." << endl;
            valid = false;
        }
    }

    cout << endl;
    
    return selection;
}


/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* The function returns the size of the menu
*
* @returns size of the menu
*
******************************************************************************/
int menu::size()
{
    return (int)theMenu.size();
}

/**************************************************************************//**
* @author Erica Keeble
*
* @par Description:
* The function clear() erases all menu items
*
******************************************************************************/
void menu::clear()
{
    theMenu.erase(theMenu.begin(), theMenu.end());
}